package com.brevitaz.training.pizzamakingprocess.pizza;

import com.brevitaz.training.pizzamakingprocess.ingredients.baseingredients.*;
import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Dough {

    private List<BaseIngredient> baseIngredients= new ArrayList<BaseIngredient>();
    public Dough(BaseIngredient[] baseIngredients){
        Collections.addAll(this.baseIngredients, baseIngredients);
    }

    @Override
    public String toString() {
        return "Dough{" +
                "baseIngredients=" + baseIngredients +
                '}';
    }
}
