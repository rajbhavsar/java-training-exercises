package com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients;

import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Onion extends Topping {
    public Onion(double weightGm) {
        super(weightGm);
    }


    @Override
    public String toString() {
        return "Onion";
    }
}
