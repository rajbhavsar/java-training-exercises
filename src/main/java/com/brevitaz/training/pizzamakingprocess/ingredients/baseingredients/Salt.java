package com.brevitaz.training.pizzamakingprocess.ingredients.baseingredients;

public class Salt extends BaseIngredient {
    public Salt(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "Salt";
    }
}
