package com.brevitaz.training.pizzamakingprocess.pizza;
import com.brevitaz.training.pizzamakingprocess.BakedFood;
import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Pizza extends BakedFood {
    private Base base;
    private double temperature;
    public Pizza(Base base, double temperature){
        this.temperature= temperature;
        this.base=base;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "base=" + base +
                ", temperature=" + temperature +
                '}';
    }
}
