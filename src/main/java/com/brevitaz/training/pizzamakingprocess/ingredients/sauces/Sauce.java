package com.brevitaz.training.pizzamakingprocess.ingredients.sauces;

import com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients.Topping;
import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Sauce extends Topping {
    public Sauce(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "Sauce";
    }
}
