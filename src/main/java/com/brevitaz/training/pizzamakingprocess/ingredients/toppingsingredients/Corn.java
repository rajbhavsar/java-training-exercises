package com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients;

import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Corn extends Topping {
    public Corn(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "Corn";
    }
}
