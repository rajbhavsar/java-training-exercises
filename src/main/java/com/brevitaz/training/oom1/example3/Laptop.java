package com.brevitaz.training.oom1.example3;

public class Laptop extends Product {
    private String model;

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }
}
