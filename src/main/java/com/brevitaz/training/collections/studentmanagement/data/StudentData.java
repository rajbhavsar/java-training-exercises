package com.brevitaz.training.collections.studentmanagement.data;

import com.brevitaz.training.collections.studentmanagement.model.Student;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class StudentData {
    private List<Student> students= new ArrayList<Student>();

    public List<Student> getStudents() {
        return students;
    }
}
