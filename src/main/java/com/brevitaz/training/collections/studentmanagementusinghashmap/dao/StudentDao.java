package com.brevitaz.training.collections.studentmanagementusinghashmap.dao;


import com.brevitaz.training.collections.studentmanagementusinghashmap.model.Student;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public interface StudentDao {
    Student insertStudent(String rollNumber,Student student);
    Student updateStudent(String rollNumber,Student newStudent);
    boolean partialUpdateByRollNumber(String rollNumber, String field, String newValue);
    boolean partialUpdateByAnyField(String field, String oldValue, String newValue);
    Student deleteStudent(String rollNumber);
    HashMap getAll();
    HashMap search(String field, String value);
}
