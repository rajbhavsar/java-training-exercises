package com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients;

import com.brevitaz.training.pizzamakingprocess.ingredients.Ingredient;
import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Topping extends Ingredient {
    private double weightGm;
    public Topping(double weightGm){
        weightGm = weightGm;
    }


    @Override
    public String toString() {
        return "Toppings";
    }
}
