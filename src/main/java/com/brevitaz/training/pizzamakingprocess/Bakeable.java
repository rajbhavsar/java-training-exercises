package com.brevitaz.training.pizzamakingprocess;

public interface Bakeable {
    public BakedFood bake(double temperature);
}
