package com.brevitaz.training.collections.studentmanagementusinghashmap.data;

import com.brevitaz.training.collections.studentmanagementusinghashmap.model.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StudentData {
//    private List<Student> students= new ArrayList<Student>();

    private HashMap<String,Student> studentHashMap=new HashMap<>();

    public HashMap<String, Student> getStudentHashMap() {
        return studentHashMap;
    }

  /*  public List<Student> getStudents() {
        return students;
    }*/
}
