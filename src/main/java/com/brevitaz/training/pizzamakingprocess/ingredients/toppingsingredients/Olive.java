package com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients;

import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Olive extends Topping {
    public Olive(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "Olives";
    }
}
