package com.brevitaz.training.pizzamakingprocess.ingredients.baseingredients;

public class Yeast extends BaseIngredient {
    public Yeast(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "yeast";
    }
}
