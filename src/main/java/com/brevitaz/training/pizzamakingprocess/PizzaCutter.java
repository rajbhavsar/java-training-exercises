package com.brevitaz.training.pizzamakingprocess;

import com.brevitaz.training.pizzamakingprocess.pizza.Pizza;

import java.util.Collection;

public class PizzaCutter {
    public Pizza cut(Pizza pizza){
        System.out.println("cutting");
        return pizza;
    }

    @Override
    public String toString() {
        return "Pizza Cutter";
    }
}
