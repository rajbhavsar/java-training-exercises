package com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients;

import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Paneer extends Topping {
    public Paneer(double weightGm) {
        super(weightGm);
    }


    @Override
    public String toString() {
        return "Paneer";
    }
}
