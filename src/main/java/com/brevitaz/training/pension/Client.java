package com.brevitaz.training.pension;

public class Client
{
	public static void main(String[] args)
	{
		PensionController pensionController= new PensionController();
		Person[] people = new Person[]{
				new Person("Tom","Jisyo","28/06/1993"),
				new Person("Jane","Doe","05/01/1950"),
				new Person("Fred","Bloggs","12/12/1949"),
		};

		pensionController.handlePension(people);
	}
}
