package com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients;

import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Capsicum extends Topping{
    public Capsicum(double weightGm) {
        super(weightGm);
    }


    @Override
    public String toString() {
        return "Capsicum";
    }
}
