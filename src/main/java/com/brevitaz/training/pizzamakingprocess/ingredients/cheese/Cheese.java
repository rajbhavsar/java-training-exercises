package com.brevitaz.training.pizzamakingprocess.ingredients.cheese;

import com.brevitaz.training.pizzamakingprocess.ingredients.toppingsingredients.Topping;
import com.brevitaz.training.pizzamakingprocess.pizza.base.Base;

public class Cheese extends Topping {
    public Cheese(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "Cheese";
    }
}
