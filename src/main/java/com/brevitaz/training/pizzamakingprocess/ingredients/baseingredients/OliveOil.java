package com.brevitaz.training.pizzamakingprocess.ingredients.baseingredients;

public class OliveOil extends BaseIngredient {
    public OliveOil(double weightGm) {
        super(weightGm);
    }

    @Override
    public String toString() {
        return "OliveOil";
    }
}
